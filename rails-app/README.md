# Rails App

To start your Ruby on Rails server:

  * Install dependencies with `bundle install`
  * Install Node.js dependencies with `yarn install --check-files`
  * Drop your database with `rails db:drop`
  * Create your database with `rails db:create`
  * Migrate your database with `rails db:migrate`
  * Start Phoenix endpoint with `rails server`

Now you can visit [`localhost:3000`](http://localhost:3000) from your browser.

Ready to run in production? Please [check our deployment guides](https://guides.rubyonrails.org).

## Learn more

  * Official website: https://rubyonrails.org/
  * Guides: https://guides.rubyonrails.org
  * Docs: https://api.rubyonrails.org/
  * Mailing list: https://rubyonrails.org/community/
  * Source: https://github.com/rails/rails