# Rails App
Install rails

https://gorails.com/setup/windows/10

## Commands
```bash
# Create project
rails new project

# Create model
rails g model profile

# Create controller
rails g controller profiles

# Scaffold
rails g scaffold profiles

# Migration
rails g migration AddFieldToProfile
```

* Add Devise: https://github.com/heartcombo/devise
* Add Bootstrap: https://dev.to/brayvasq/integrate-andminlte-with-ruby-on-rails-6-od7

**Devise commands**
```bash
rails generate devise:install
rails generate devise user
rails generate devise:views
```
