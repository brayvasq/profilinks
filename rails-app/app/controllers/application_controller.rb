class ApplicationController < ActionController::Base
    before_action :authenticate_user!

    def after_sign_in_path_for(resource)
      profiles_path
    end

    protected
    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:account_update, keys: [:name, :description])
    end
end
