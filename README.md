# Profilinks
This repository will contains some mini link.tree clones in multiple backend frameworks.

## List
- `rails-app/` Contains the Ruby on Rails implementation.
- `phoenix-app/` Contains the Elixir Phoenix implementation.
- `amber-app/` Contains the Crystla Amber implementation.
- `django-app/` Contains the Python Django implementation.
