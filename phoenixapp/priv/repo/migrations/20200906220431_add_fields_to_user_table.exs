defmodule Phoenixapp.Repo.Migrations.AddFieldsToUserTable do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :name, :text
      add :description, :text
    end
  end
end
