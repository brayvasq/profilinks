# Phoenix App
Install rails: https://hexdocs.pm/phoenix/installation.html

## Commands
```bash
# Create project
mix phx.new phoenixapp

# Routes
mix phx.routes

# Create db
mix ecto.drop

# Create db
mix ecto.create

# Run migrations
mix ecto.migrate

# Generate model
mix phx.gen.schema Blog.Post blog_posts title:string views:integer

# Generate scaffold
mix phx.gen.html Users Profile profiles name:text url:text user_id:references:users

# Generate migration
mix ecto.gen.migration update_posts_table
```

* Add Pow: https://powauth.com/
* Sheatsheet: https://devhints.io/phoenix-migrations
* Scaffold Tutorial: https://nithinbekal.com/posts/elixir-phoenix-crud-app/

**Pow Commands**
```bash
mix pow.install
mix pow.phoenix.gen.templates
```
