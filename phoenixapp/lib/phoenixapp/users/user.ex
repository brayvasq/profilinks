defmodule Phoenixapp.Users.User do
  use Ecto.Schema
  use Pow.Ecto.Schema

  schema "users" do
    field :name, :string
    field :description, :string

    pow_user_fields()

    timestamps()
  end
end
