defmodule PhoenixappWeb.Pow.Routes do
  use Pow.Phoenix.Routes
  alias PhoenixappWeb.Router.Helpers, as: Routes

  def after_sign_in_path(conn), do: Routes.profile_path(conn, :index)
end
