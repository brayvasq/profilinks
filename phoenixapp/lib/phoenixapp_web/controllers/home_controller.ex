defmodule PhoenixappWeb.HomeController do
  use PhoenixappWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end    
end
