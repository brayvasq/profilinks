-- +micrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE users ADD COLUMN name VARCHAR;
ALTER TABLE users ADD COLUMN description VARCHAR;

-- +micrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE users DROP COLUMN name VARCHAR;
ALTER TABLE users DROP COLUMN description VARCHAR;
