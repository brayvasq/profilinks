-- +micrate Up
CREATE TABLE profiles (
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR,
  url VARCHAR,
  user_id BIGINT,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);
CREATE INDEX profile_user_id_idx ON profiles (user_id);

-- +micrate Down
DROP TABLE IF EXISTS profiles;
