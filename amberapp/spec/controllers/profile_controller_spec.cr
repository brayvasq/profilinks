require "./spec_helper"

def profile_hash
  {"name" => "Fake", "url" => "Fake", "user_id" => "1"}
end

def profile_params
  params = [] of String
  params << "name=#{profile_hash["name"]}"
  params << "url=#{profile_hash["url"]}"
  params << "user_id=#{profile_hash["user_id"]}"
  params.join("&")
end

def create_profile
  model = Profile.new(profile_hash)
  model.save
  model
end

class ProfileControllerTest < GarnetSpec::Controller::Test
  getter handler : Amber::Pipe::Pipeline

  def initialize
    @handler = Amber::Pipe::Pipeline.new
    @handler.build :web do
      plug Amber::Pipe::Error.new
      plug Amber::Pipe::Session.new
      plug Amber::Pipe::Flash.new
    end
    @handler.prepare_pipelines
  end
end

describe ProfileControllerTest do
  subject = ProfileControllerTest.new

  it "renders profile index template" do
    Profile.clear
    response = subject.get "/profiles"

    response.status_code.should eq(200)
    response.body.should contain("profiles")
  end

  it "renders profile show template" do
    Profile.clear
    model = create_profile
    location = "/profiles/#{model.id}"

    response = subject.get location

    response.status_code.should eq(200)
    response.body.should contain("Show Profile")
  end

  it "renders profile new template" do
    Profile.clear
    location = "/profiles/new"

    response = subject.get location

    response.status_code.should eq(200)
    response.body.should contain("New Profile")
  end

  it "renders profile edit template" do
    Profile.clear
    model = create_profile
    location = "/profiles/#{model.id}/edit"

    response = subject.get location

    response.status_code.should eq(200)
    response.body.should contain("Edit Profile")
  end

  it "creates a profile" do
    Profile.clear
    response = subject.post "/profiles", body: profile_params

    response.headers["Location"].should eq "/profiles"
    response.status_code.should eq(302)
    response.body.should eq "302"
  end

  it "updates a profile" do
    Profile.clear
    model = create_profile
    response = subject.patch "/profiles/#{model.id}", body: profile_params

    response.headers["Location"].should eq "/profiles"
    response.status_code.should eq(302)
    response.body.should eq "302"
  end

  it "deletes a profile" do
    Profile.clear
    model = create_profile
    response = subject.delete "/profiles/#{model.id}"

    response.headers["Location"].should eq "/profiles"
    response.status_code.should eq(302)
    response.body.should eq "302"
  end
end
