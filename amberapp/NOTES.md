# Amber App
Install amber: https://docs.amberframework.org/amber/getting-started

## Commands
```bash
# Create project
amber new pet-tracker

# Create database
amber db create

# Migrations
amber db migrate

# Auth
amber g auth User

# Migrations
touch bin/micrate
chmod +x bin/micrate
bin/micrate create add_fields_to_user
## Rename to have a valid timestamp

# Scaffold
amber g scaffold Profile name:string url:string user:references
```

* Migrations Docs: https://docs.amberframework.org/granite/docs/migrations
* Micrate: https://github.com/amberframework/micrate
