class Profile < Granite::Base
  connection pg
  table profiles

  belongs_to :user

  column id : Int64, primary: true
  column name : String?
  column url : String?
  timestamps
end
