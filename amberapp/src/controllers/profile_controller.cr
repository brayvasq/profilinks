class ProfileController < ApplicationController
  getter profile = Profile.new

  before_action do
    only [:show, :edit, :update, :destroy] { set_profile }
    only [:show, :edit, :update, :destroy] { set_user }
  end

  def index
    profiles = Profile.where(user_id: @user.id)
    render "index.slang"
  end

  def show
    render "show.slang"
  end

  def new
    render "new.slang"
  end

  def edit
    render "edit.slang"
  end

  def create
    profile = Profile.new profile_params.validate!
    profile.user_id = @user.id

    if profile.save
      redirect_to action: :index, flash: {"success" => "Profile has been created."}
    else
      flash[:danger] = "Could not create Profile!"
      render "new.slang"
    end
  end

  def update
    profile.set_attributes profile_params.validate!
    if profile.save
      redirect_to action: :index, flash: {"success" => "Profile has been updated."}
    else
      flash[:danger] = "Could not update Profile!"
      render "edit.slang"
    end
  end

  def destroy
    profile.destroy
    redirect_to action: :index, flash: {"success" => "Profile has been deleted."}
  end

  private def profile_params
    params.validation do
      required :name
      required :url
    end
  end

  private def set_profile
    @profile = Profile.find! params[:id]
  end

  private def set_user
    @user = current_user.not_nil!
  end
end
